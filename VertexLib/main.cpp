#include "main.h"

#include "AsaVertex.h"

int main(int argc, char *argv[]){

	Matrix a = Matrix::rotX(90);
	a.X[3] = -50;
	Vertex3D b(10, 0, 5);
	Vertex3Df c(10, 0, 5);
	cout << a << endl;
	cout << b << endl;
	cout << c << endl << endl;

	cout << a*b << endl;
	cout << a*c << endl;
	cout << a.inv() * a * b << endl;

	getchar();
	return 0;
}
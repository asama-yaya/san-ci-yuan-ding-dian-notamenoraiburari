#include "AsaVertex.h"


//****************************************************************************
// N*M dobule Matrix Class
//****************************************************************************
// 行列式
double Matrix::det(void){
	int *ip;
	Matrix src(*this);
	double result;
	ip = new int[col];
	result = MathCalMethod::lu(src.col, src.X, ip);
	delete[]ip;
	return result;
}

// 転置
Matrix Matrix::trn(){
	Matrix result(col, row);
	for (int i = 0; i < row; i++)
	for (int j = 0; j < col; j++){
		result.X[j*row + i] = X[i*col + j];
	}
	return result;
}

// 逆行列
Matrix Matrix::inv(){
	Matrix src = *this;
	Matrix dst(col, row);
	MathCalMethod::Inverse_LU(dst.X, src.X, col);
	//	if(Inverse_LU(dst.X,src.X,n)==0.0);
	//cout<<"Error03:Faile to caliculate inverse matrix."<<endl;
	return dst;
}

// メモリ開放。デストラクタで開放してるので基本使わない
void Matrix::free()
{
	if (this->col*this->row)
		delete[]X;
	this->col = this->row = 0;
}

// メモリ確保。コンストラクタでやってるので基本使わない
void Matrix::malloc(int _n, int _m){
	row = _n;
	col = _m;
	X = new double[row*col];
	for (int i = 0; i<row*col; i++)
		X[i] = 0;
}

// 単位行列に初期化
void Matrix::uni()
{
	for (int i = 0; i<this->row*this->col; i++)
		this->X[i] = 0;
	for (int i = 0; i<this->row && i<this->col; i++)
		this->X[col*i + i] = 1;
}

//****************************************************************************
// Vector and Matrix Calculation Function
//****************************************************************************
// LU分解
double MathCalMethod::lu(int n, double *a, int *ip){
	int i, j, k, ii, ik;
	double t, u;
	double *weight = new double[n];

	// 行列式
	double det = 0;

	// 各行について
	for (k = 0; k < n; k++) {
		ip[k] = k;             /* 行交換情報の初期値 */
		u = 0;                 /* その行の絶対値最大の要素を求める */
		for (j = 0; j < n; j++) {
			t = fabs(a[n*k + j]);  if (t > u) u = t;
		}
		if (u == 0) goto EXIT; /* 0 なら行列はLU分解できない */
		weight[k] = 1 / u;     /* 最大絶対値の逆数 */
	}

	// 行列式の初期値
	det = 1;

	// 各行について
	for (k = 0; k < n; k++) {
		u = -1;
		for (i = k; i < n; i++) {  /* より下の各行について */
			ii = ip[i];            /* 重み×絶対値 が最大の行を見つける */
			t = fabs(a[n*ii + k]) * weight[ii];
			if (t > u) { u = t;  j = i; }
		}
		ik = ip[j];
		if (j != k) {
			ip[j] = ip[k];  ip[k] = ik;  /* 行番号を交換 */
			det = -det;  /* 行を交換すれば行列式の符号が変わる */
		}
		u = a[n*ik + k];  det *= u;  /* 対角成分 */
		if (u == 0) goto EXIT;    /* 0 なら行列はLU分解できない */
		for (i = k + 1; i < n; i++) {  /* Gauss消去法 */
			ii = ip[i];
			t = (a[n*ii + k] /= u);
			for (j = k + 1; j < n; j++)
				a[n*ii + j] -= t * a[n*ik + j];
		}
	}
EXIT:
	delete[]weight;
	return det;
}

double MathCalMethod::Inverse_LU(double *a_inv, double *a, int n){
	int i, j, k, ii;
	double t, det;
	int *ip = new int[n];
	det = lu(n, a, ip);
	if (det != 0)
	for (k = 0; k < n; k++) {
		for (i = 0; i < n; i++) {
			ii = ip[i];  t = (ii == k);
			for (j = 0; j < i; j++)
				t -= a[n*ii + j] * a_inv[n*j + k];
			a_inv[n*i + k] = t;
		}
		for (i = n - 1; i >= 0; i--) {
			t = a_inv[n*i + k];  ii = ip[i];
			for (j = i + 1; j < n; j++)
				t -= a[n*ii + j] * a_inv[n*j + k];
			a_inv[n*i + k] = t / a[n*ii + i];
		}
	}
	delete[]ip;
	return det;
}

double MathCalMethod::Inverse_Gauss(Matrix *inv, Matrix *a){
	int i, j, k;
	double t, u, det = 1;
	for (i = 0; i<a->col*a->row; i++)inv->X[i] = a->X[i];

	for (k = 0; k < a->col; k++) {
		t = inv->X[a->col*k + k];  det *= t;
		for (i = 0; i < a->col; i++) inv->X[a->col*k + i] /= t;
		inv->X[a->col*k + k] = 1 / t;
		for (j = 0; j < a->col; j++)
		if (j != k) {
			u = inv->X[a->col*j + k];
			for (i = 0; i < a->col; i++)
			if (i != k) inv->X[a->col*j + i] -= inv->X[a->col*k + i] * u;
			else        inv->X[a->col*j + i] = -u / t;
		}

	}
	return det;
}

double MathCalMethod::nABS(double *_src, int _len){
	double dst = 0;
	for (int i = 0; i<_len; i++)dst += _src[i] * _src[i];
	if (dst != 0)dst = sqrt(dst);
	return dst;
}